# Contributing
This project forms the submission for CM30082 Dissertation, MComp Comp.Sci. w/ Maths. for Adam Lassiter, University of Bath, UK, 2017-18.
For this reason, contributing is prohibited.
