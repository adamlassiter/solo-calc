\subsection{Solo Calculus}
    
    Developed by Cosimo Laneve and Bj{\"o}rn Victor in the early 2000s, the solo calculus aims to be an improvement of the Fusion calculus.
    As such, there exists an encoding of the Fusion calculus within the solo calculus (and hence an encoding of the $\pi$-calculus).
    The name comes from the strong distinction between the components of the calculus: \textit{solos} and \textit{agents}.
    These are roughly analogous to input/output actions and a calculus syntax similar to the $\lambda$-calculus.
    Through some clever design choices, the solo calculus is found to have some interesting properties over other process calculi.

    \begin{definition}{(Syntax)\\}
        \label{solo-calculus-syntax}
        As defined by~\cite{solo-calculus}, the solo calculus is constructed from \textit{solos} ranged over by $\alpha, \beta \ldots$ and \textit{agents} ranged over by $P, Q \ldots$ as such:
        \begin{center}
            \begin{tabular}{ l l l }
                $\alpha \quad \defeq$   & $u \, \tilde{x}$          & (input) \\
                                        & $\bar{u} \, \tilde{x}$    & (output)~\footnotemark\\ \\
                $P \quad \defeq$        & $0$                       & (inaction) \\
                                        & $\alpha$                  & (solo) \\
                                        & $Q \, | \, R$             & (composition) \\
                                        & $(x) \, Q$                & (scope) \\
                                        & $[x=y] \, Q$              & (match) \\
                                        & $!\,P$                    & (replication)
            \end{tabular}
        \end{center}\footnotetext{$\tilde{x}$ is used as shorthand for any tuple $(x_1 \ldots x_n)$.}
        where the scope operator $(x) \, P $ is a declaration of the named variable $x$ in $P$.
        This ensures that $x$ is local to $P$, even if it assigned outside of $P$ (ex. $(x \, y | (x) \, P)$ will never have $x \defeq y$ unless explicitly assigned such in P).
    \end{definition}
    This is a much more minimal syntax when compared to CCS (and certainly Higher-Order CCS as described by~\cite{pi-calculus-in-ccs}).
    It will further be seen that the reduction rules retain this simplicity.
    It should be noted that the names $u, x, etc\ldots$ within a solo may be treated as both channel names and as values.


    \begin{definition}{(Match Operator)\\}
        The match operator $[x \, = \, y] \, P$ computes $P$ if $x$ and $y$ are the same name, otherwise computes \textbf{0}.
        These match operators are iterated over here by $M, N$, with sequences of match operators iterated over by $\tilde{M}, \tilde{N}$.
        Each name occurring in $M$ is a \textit{labelled node} of $M$.
    \end{definition}


    \begin{definition}{(Structural Congruence)\\}
        The structural congruence relation $\equiv$ in the solo calculus is exactly that defined in Definition~\ref{fusion-calculus-structural-congruence}.
    \end{definition}


    \begin{definition}{(Reduction)\\}
        Reduction semantics on solo expressions are defined as:
        \begin{align}
            (x)(\bar{u} \, x \, | \, u \, y \, | \, P) \rightarrow & \, P\{y / x\} \\
            P \rightarrow P' \implies &
            \begin{cases}
                P \, | \, Q \rightarrow P' \, | \, Q \\
                (x) \, P \rightarrow (x) \, P' \\
                P \equiv Q \text{ and } P' \equiv Q' \implies Q \rightarrow Q'
            \end{cases}
        \end{align}
        where $P\{y / x\}$ is $\alpha$-substitution of the name $x$ to the name $y$.
    \end{definition}
    It is interesting to note here the asynchronous behaviour of the solo calculus.
    Where in the $\pi$-calculus and CCS input/output actions where synchronised and preceded processes as guards, the solo calculus naturally treats all agents as unguarded and names may be substituted whenever is desired.


    \begin{remark*}
        There exists an encoding of the Fusion calculus within the solo calculus.
        This can most easily be seen as an encoding of the choice-free Fusion calculus as a combination of the above syntax and semantics of the solo calculus and also the prefix operator $\alpha \, . \, P$\footnote{For further details,~\cite{solo-calculus} discuss this implementation in Section 3}.
        Hence there exists an encoding of the $\pi$-calculus also, complete with the same style of guarded input/output communication.
    \end{remark*}





\subsection{Solo Diagrams}
    The solo calculus was further developed by~\cite{solo-diagrams} to provide a one-to-one correspondence between these  expressions and `diagram-like' objects.
    This provides a strong analog to real-world systems and an applicability to be used as a modelling tool for groups of communicating systems.
    Furthermore, as discussed by~\cite{learning-styles}, a visual output of information is often found to be preferable for cognition than verbal or textual information.


    \begin{definition}{(Edge)\\}
        An edge is defined to be:
        \begin{align}
            E \defeq \, \langle a, a_1 \ldots a_k\rangle_t \quad \text{for } t \in \{ i, o \}
        \end{align}
        where $a, a_i$ are \textit{nodes}, $\langle \ldots \rangle_i$ is an \textit{input edge}, $\langle \ldots \rangle_o$ is an \textit{output edge} and $k$ the edge's \textit{arity}.
    \end{definition}

    \begin{figure}[H]
        \centering
        \begin{subfigure}{0.4\linewidth}
            \centering
            \begin{tikzpicture}[transform shape, every node/.style={circle, fill=black!100, inner sep=0.05cm}]
                \node[anchor=center, label=below:{$a$}](a){};
                \coordinate[right=1cm of a](ax);
                \node[above right=1cm of ax, label=below:{$a_1$}](a1){};
                \node[below right=1cm of ax, label=below:{$a_2$}](a2){};
                \draw[-{>[scale=2]}] (ax) -- (a);
                \draw[-] (a1) -- (ax) -- (a2);
            \end{tikzpicture}
            \caption*{Output edge $\langle a, a_1, a_2\rangle_o$}
        \end{subfigure}
        \begin{subfigure}{0.4\linewidth}
            \centering
            \begin{tikzpicture}[transform shape, every node/.style={circle, fill=black!100, inner sep=0.05cm}]
                \node[anchor=center, label=below:{a}](a){};
                \coordinate[right=1cm of a](ax);
                \node[above right=1cm of ax, label=below:{$a_1$}](a1){};
                \node[below right=1cm of ax, label=below:{$a_2$}](a2){};
                \draw[-{<[scale=2]}] (ax) -- (a);
                \draw[-] (a1) -- (ax) -- (a2);
            \end{tikzpicture}
            \caption*{Input edge $\langle a, a_1, a_2\rangle_i$}
        \end{subfigure}
    \end{figure}

    This is analogous to an input or output solo in the calculus, where $a$ is $u$ or $\bar{u}$ and $a_1 \ldots a_n$ is $\tilde{x}$ as written in Definition~\ref{solo-calculus-syntax}.
    Note that inputs and outputs must have matching arity --- a 2-arity input cannot communicate with a 3-arity output for obvious reasons.


    \begin{definition}{(Box)\\}
        A box is defined to be:
        \begin{align}
            B \defeq \, \langle G, S \rangle \quad \text{for } S \subset nodes(G)\footnotemark
        \end{align}\footnotetext{This is written as shorthand for all nodes contained within a given object, in this case $\{a \text{ s.t. } a \in nodes(S),\, S \in G \}$}
        where G is a \textit{graph} (or multiset of \textit{edges}) and S is a set of \textit{nodes}, referred to as the \textit{internal nodes} of $B$.
        The \textit{principal nodes} of $B$ are then $nodes(G) \setminus S$.
    \end{definition}

    \begin{figure}[H]
        \centering
        \begin{subfigure}{0.4\linewidth}
            \centering
            \begin{tikzpicture}[transform shape, every node/.style={circle, fill=black!100, inner sep=0.05cm}]
                \coordinate[anchor=center](nw);
                \coordinate[below=3cm of nw](sw);
                \coordinate[right=3cm of nw](ne);
                \coordinate[below right=3cm and 3cm of nw](se);
                \draw[-] (nw) -- (ne) -- (se) -- (sw) -- (nw);
                \node[right=1.5cm of sw, label=below:{$x$}](x){};
                \node[below=1.5cm of ne, label=right:{$y$}](y){};
                \node[below right=1.5cm of nw, label=above left:{$w$}](w){};
                \coordinate[below right=1cm of w](wxyx){};
                \draw[-] (y) -- (wxyx) -- (w);
                \draw[-{<[scale=2]}] (wxyx) -- (x);
                \draw[-{>[scale=2]}] (y) to [out=140, in=10] (w);
            \end{tikzpicture}
            \caption*{Box representing $!(w)(x \, w y \, | \, \tilde{w} \, y)$}
        \end{subfigure}\footnotemark
    \end{figure}\footnotetext{Usually the $w$ in the diagram would be excluded, but is included here for illustration purposes only.}

    This can then be seen to be analogous to the replication operator, with the idea being that the principal nodes form the perimeter of a box and cannot be replicated --- they serve as the interface to the internals of the box.


    \begin{definition}{(Diagram)\\}
        A solo diagram is defined to be:
        \begin{align}
            SD \defeq (G, M, \ell)
        \end{align}
        where $G$ is a finite multiset of \textit{edges}, $M$ is a finite multiset of \textit{boxes} and $\ell$ a labelling of the $nodes(G)$ and of $principals(M)$.
    \end{definition}
    From here, we can convert solo calculus to diagrams, where composition is intuitively just including two separate diagrams together and scope is simply any connected nodes labelled by $\ell$.
    There are then four required reduction cases (edge-edge, edge-box, box-box and box internals) which can be deduced from the definition of the calculus.


    \begin{definition}{(Diagram Reduction)\\}
        Let $G, G_1, G_2 \ldots$ be arbitrary graphs, $M, M'$ arbitrary box multisets, $\alpha \defeq \langle a, a_1 \ldots a_k \rangle_i$, $\beta \defeq \langle a, a_1' \ldots a_k' \rangle_o$, $\sigma \defeq a_i \mapsto a_i'$, $\rho$ a arbitrary but fresh relabelling and $G\sigma$ shorthand for $G[\sigma]$ the application of the renaming $\sigma$ on the edges of G.
        $\alpha$ and $\beta$ need not be fixed to input and output respectively, but must be opposite polarity.
        Then, the following reductions may be made:
        \begin{align}
            (G \cup \{\alpha, \beta\}, M, \ell)                                                                                     & \rightarrow (G\sigma, M\sigma, \ell') \\
            (G_1 \cup \{\alpha\}, M \defeq \langle G_2 \cup \{\beta\}, S \rangle, \ell)                                             & \rightarrow ((G_1 \cup G_2\rho)\sigma, M\sigma, \ell') \\
            (G, M \defeq \{\langle \{ \alpha \} \cup G_1, S_1 \rangle,\langle \{ \beta \} \cup G_2, S_2 \rangle\} \cup M', \ell)    & \rightarrow ((G \cup G_1\rho \cup G_2\rho)\sigma, M\sigma, \ell') \\
            (G, M \defeq \langle \{ \alpha, \beta \} \cup G_1, S \rangle \cup M', \ell)                                             & \rightarrow ((G \cup G_1\rho)\sigma, M\sigma, \ell')
        \end{align}
        where each represents reduction of an edge-edge, edge-box, box-box and of box internals respectively.
    \end{definition}


    \begin{example*}
        \begin{figure}[H]
            \centering
            \begin{subfigure}{0.4\linewidth}
                \centering
                \begin{tikzpicture}[transform shape, every node/.style={circle, fill=black!100, inner sep=0.05cm}]
                    \coordinate[anchor=center](nw);
                    \coordinate[below=3cm of nw](sw);
                    \coordinate[right=3cm of nw](ne);
                    \coordinate[below right=3cm and 3cm of nw](se);
                    \draw[-] (nw) -- (ne) -- (se) -- (sw) -- (nw);
                    \node[right=1.5cm of sw, label=below left:{$x$}](x){};
                    \coordinate[below=1cm of x](xba){};
                    \coordinate[above=1cm of x](xaa){};
                    \node[above left=1cm of xaa](v){};
                    \node[above right=1cm of xaa](w){};
                    \node[below left=1cm of xba, label=left:{$y$}](y){};
                    \node[below right=1cm of xba, label=right:{$z$}](z){};
                    \draw[-{<[scale=2]}] (xaa) -- (x);
                    \draw[-{>[scale=2]}] (xba) -- (x);
                    \draw[-{>[scale=2]}] (w) to [out=140, in=40] (v);
                    \draw[-] (w) -- (xaa) -- (v);
                    \draw[-] (y) -- (xba) -- (z);
                \end{tikzpicture}
                \caption*{$\bar{x}\, y z \, | \, !(uv)(x \, u v \, | \, \bar{u} \, v)$}
            \end{subfigure}
            $\longrightarrow$
            \begin{subfigure}{0.4\linewidth}
                \centering
                \begin{tikzpicture}[transform shape, every node/.style={circle, fill=black!100, inner sep=0.05cm}]
                    \coordinate[anchor=center](nw);
                    \coordinate[below=3cm of nw](sw);
                    \coordinate[right=3cm of nw](ne);
                    \coordinate[below right=3cm and 3cm of nw](se);
                    \draw[-] (nw) -- (ne) -- (se) -- (sw) -- (nw);
                    \node[right=1.5cm of sw, label=below left:{$x$}](x){};
                    \coordinate[below=1cm of x](xba){};
                    \coordinate[above=1cm of x](xaa){};
                    \node[above left=1cm of xaa](v){};
                    \node[above right=1cm of xaa](w){};
                    \node[below left=1cm of xba, label=left:{$y$}](y){};
                    \node[below right=1cm of xba, label=right:{$z$}](z){};
                    \draw[-{<[scale=2]}] (xaa) -- (x);
                    \draw[-{>[scale=2]}] (w) to [out=140, in=40] (v);
                    \draw[-] (w) -- (xaa) -- (v);
                    \draw[-{>[scale=2]}] (z) -- (y);
                \end{tikzpicture}
                \caption*{$\bar{y} \, z \, | \, !(uv)(x \, u v \, | \, \bar{u} \, v)$}
            \end{subfigure} 
        \end{figure}
    \end{example*}


    \begin{remarks}
        The solo calculus is found to be simple, expressive and remarkable in its capability to be visualised as a diagram.
        For further reading,~\cite{acyclic-solos} present in great detail the topics of the $\pi$ and solo calculus, solo diagrams and furthermore differential interaction nets.
    \end{remarks}


