\subsection{Fusion Calculus}

    Bj{\"o}rn Victor and Joachim Parrow first designed the Fusion calculus in the late 1990's as an attempt to simplify the $\pi$-calculus further still.
    They reduced the number of binding operators from two (input/output) to one (fusion).
    This new operation assigned two names to the same value or channel in one of the processes they were in, meaning the calculus has the bizarre property that all input/output actions are symmetric.
    The reduction in operators meant a reduction in bisimulation congruences, down now from three to just one, which justifies further mention here.
    
    \begin{definition}{(Syntax)\\}
        As defined by~\cite{fusion-calculus}, the Fusion calculus is composed from \textit{free actions} ranged over by $\alpha \ldots$ and \textit{agents} ranged over by $P, Q \ldots$ as such:
        \begin{center}
            \begin{tabular}{ l l l }
                $P \quad \defeq$    & $\textbf{0}$      & (null process) \\
                                    & $\alpha . P$      & (action prefix) \\
                                    & $P \, | \, Q$     & (concurrency) \\
                                    & $P + Q$           & (choice) \\
                                    & $(x) \, P$        & (scope) \\ \\

                $\alpha \quad \defeq$   & $x y$         & (input) \\
                                        & $\bar{x} y$   & (output) \\
                                        & $\phi$        & (fusion) \\
            \end{tabular}
        \end{center}
        where $x$ is bound in $(x)P$.
    \end{definition}


    \begin{definition}{(Structural Congruence)\\}
        \label{fusion-calculus-structural-congruence}
        The structural congruence $\equiv$ is the least congruence relation satisfying $\alpha$-equivalence, associativity, commutativity, \textbf{0} identity and the following scope laws:
        \begin{align}
            (x) \, \textbf{0}       & \equiv \textbf{0} \\
            (x) \, (y) \, P         & \equiv (y) \, (x) \, P \\
            (x) \, (P \, | \, Q)    & \equiv P \, | \, (x)Q \quad \text{where $z \notin fn(P)$} \\
            (x)(P \, + \, Q)        & \equiv (x)P \, + \, (x)Q
        \end{align}
        This aims to provide the core definition of equivalent computation and a basis for reductions.
    \end{definition}


    \begin{definition}{(Semantics)\\}
        Similar to CCS, a reduction in the Fusion calculus is a labelled transition $P \xrightarrow{\alpha} Q$ where:
        where each satisfies the expression:
        \begin{align}
            \alpha . P \xrightarrow{\alpha}                             & P \\
            P \xrightarrow{ux} P', Q \xrightarrow{\bar{u}y} Q' \implies & P \, | \, Q \xrightarrow{x = y} P' \, | \, Q' \\
            P \xrightarrow{\alpha} P' \implies                          &
            \begin{cases}
                P \, | \, Q \xrightarrow{\alpha} P' \, | \, Q \\
                P + Q \xrightarrow{\alpha} P' \\
            \end{cases} \\
            P \xrightarrow{\phi} P', z \phi x, z \neq x \implies                            & (x)P \xrightarrow{\phi \backslash z} P'{x / z} \\
            P \xrightarrow{\alpha} P', x \notin names(\alpha) \implies                      & (x)P \xrightarrow{\alpha} (x)P' \\
            P \equiv P', Q \equiv Q', P \xrightarrow{\alpha} Q \implies                     & P \xrightarrow{\alpha} Q \\
            P \xrightarrow{(y) a x} P', z \in \{x, y\}, a \notin \{z, \bar{z}\} \implies    & (z)P \xrightarrow{(z y) a x} P'
        \end{align}
    \end{definition}
    Note that all of the above can be easily generalised for action objects being k-length tuples rather than single values.


    \begin{example*}
        Within the Fusion calculus --- like the $\pi$-calculus --- input and output actions block computation of a process until the input/output action is performed.
        However, we may define an asynchronous, delayed input that allows $P$ to continue with computation before binding an inputted value $x$ from a channel $u$.
        \begin{align*}
            u(x): P \, \defeq \, (x)(ux \, | \, P)
        \end{align*}
    \end{example*}

    
    \begin{remarks}
        There exists an encoding of the $\pi$-calculus within the Fusion calculus.
        This is not so interesting at this point but is later. \\
        The Fusion calculus shows that simplicity is definitely desirable in a process calculus as the reduction of rules and syntax eases readability and aids in proving properties of expressions.
        Despite this, \textit{I believe it still does not express suitably well the function of an expression without applying much thought or computation}.
    \end{remarks}
