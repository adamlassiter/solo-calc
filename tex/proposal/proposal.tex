\documentclass{article}

\input{../preamble}


\title{A Graphical Representation of the Solos Calculus \\
	\large CM30082 Project Proposal}

\author{Adam Lassiter}

\date{October 2017}


\begin{document}

\maketitle

\vfill

\tableofcontents

\pagebreak


\section{Problem Description}
\subsection{Current Problem}
As computer processor clock speeds have begun to stagnate in performance increase per year, manufacturers have begun to shift focus to multiple cores in search of greater performance. Supercomputers for decades have operated around the idea of clusters and massive parallelism. For this reason there is now more than ever a need to study the mathematical effects of concurrency in computation.

\subsection{Existing Solutions}
\subsubsection{$\lambda$-calculus}\label{ssubsec:solns_lambda_calc}
Developed by Alonzo Church in the 1930s, the $\lambda$-calculus is a more mathematical representation of a computable function. It forms a universal model of computation and can contain an encoding of any single-taped Turing machine. The $\lambda$-calculus became a popular formal system within which to study properties of computation.
The $\lambda$-calculus consists of an expression $M$ built from the following terms:
\begin{equation*}
	M  \quad::=\quad  a  \quad|\quad  (\lambda x . M)  \quad|\quad  (M N)
\end{equation*}
where the expressions describe a variable, an abstraction and an application of a function on an argument. From this, any computable function can be constructed and computation is achieved through a series of reductions.
\\
While the $\lambda$-calculus has been successful and been studied by various areas of academia outside of Computer Science, it is limited in application by its fundamentally `single-process' model and struggles to describe multiple systems working and communicating together.

\subsubsection{$\pi$-calculus}
Similar to the $\lambda$-calculus as described in~\ref{ssubsec:solns_lambda_calc}, there exists the $\pi$-calculus for the study of concurrent computation.
\\
The $\pi$-calculus is constructed from the recursive definition of  an expression $P$:
\begin{equation*}
	P  \quad::=\quad  P|P  \quad|\quad  c(x).P  \quad|\quad  \bar{c}(x).P  \quad|\quad  vx.P  \quad|\quad  !P  \quad|\quad  0
\end{equation*}
where the expressions describe each of concurrency, waiting for input, providing output, name binding (similar to $\lambda x . P$), replication (which itself is $!P \coloneq P|!P$) and process termination respectively.
While it provides the expressiveness required for Turing-completeness, it does not lend itself to understandability nor clarity of the problem encoding when presented as a standalone expression.

\subsubsection{Solos Calculus}
As defined by~\cite{solo-concert}, the Solos calculus is constructed from \textit{solos} ranged over by $\alpha,\beta,\ldots$ and \textit{agents} ranged over by $P,Q,\ldots$ as such:
\begin{center}
	\begin{tabular}{ l l l }
		$\alpha ::=$	& $u\tilde{x}$		& (input) \\
				& $\bar{u}\tilde{x}$	& (output) \\ \\
		$P ::=$		& $0$			& (inaction) \\
				& $\alpha$		& (solo) \\
				& $Q | R$		& (composition) \\
				& $(x)Q$		& (scope) \\
				& $[x=y]Q$		& (match) \\
	\end{tabular}
\end{center}
This was further developed by~\cite{solo-diagrams} to provide a one-to-one correspondence between these  expressions and `diagram-like' objects. This provides a strong analog to real-world systems and an applicability to be used as a modelling tool for groups of communicating systems.

\subsubsection{Others}
There also exist many further variants, such as the Fusion calculus --- itself a superset of $\pi$-calculus --- and CSP (or communicating sequential processes). These aim to improve on the $\pi$-calculus in making the problem encoding more understandable, but at the cost of a more complicated language, making them harder to implement and prove reductions in.


\subsection{Project Aims}
The project seeks to provide an implementation of both the Solos calculus, for which there exists an encoding of the $\pi$-calculus within itself, and also of Solos diagrams, an intuitively graphical representation of the calculus. These diagrams can then aid in understanding of reductions of process calculi, while maintaining a clean and simple language with strong provable properties.
Furthermore, the design of the Solos calculus is such that it is fully asynchronous. That is, unlike in the $\pi$-calculus where processes `block' while waiting for inputs/outputs, it is shown through the construction of the Solos calculus that no such system is required --- however there still exists a way of building such a system to give the effects of the $\pi$-calculus should it be desired.
For these reasons, the Solos calculus is found to be an interesting alternative to the more common $\pi$-calculus and is therefore chosen as the project focus.

\subsection{Objectives}
Throughout the project, the following objectives and landmarks are hoped to be achieved:
\begin{itemize}
	\item Research current models of concurrency calculus and understand common use-cases and environments.
	\item Build a system capable of computing Solos calculus expressions.
	\item Be able to convert expressions to diagrams.
	\item Visualise and interact with/modify diagrams and watch reductions step-by-step.
	\item Evaluate qualities of the system as a learning tool.
\end{itemize}


\section{Requirements Specification}
The following seeks to provide an estimate for the progress path the project will take, judging by initial research and planning. These are likely to evolve over time, as are priorities of each requirement (\textit{must $>$ should $>$ may $>$ might}).

\subsection{Solos Expressions}\label{subsec:reqs_solo_expr}
The first functional part of the system will be focused on text-based Solos Expressions only. This will form a basis and back-end to build upon to implement other features.
This system:
\begin{itemize}
	\item \textit{Must} support input and output of text representing Solos Expressions.
	\item \textit{Must} be able to perform reductions on inputted expressions and return them.
	\item \textit{Should} provide this functionality through an accessible user interface.
	\item \textit{May} be implemented to evaluate expressions concurrently.
\end{itemize}

\subsection{Solos Diagrams}
This will be the first extension of the system to allow interoperability with Solos Diagrams, an equivalent representation of the calculus, which has intuitive graphical representations.
This system:
\begin{itemize}
	\item \textit{Must} support input and output of said Solos Diagrams.
	\item \textit{Must} provide a conversion between Diagrams and Expressions.
	\item \textit{Should} be able to provide the same reductions as in the Expressions.
	\item \textit{May} be able to produce step-by-step reductions.
	\item \textit{Might} be implemented to evaluate diagrams concurrently.
\end{itemize}

\subsection{Diagram Visualisation and Interaction}
The second extension will build on the previous to provide a graphical interface for input/output/manipulation of Solos Diagrams giving a clear visual representation of how the calculus works.
This system:
\begin{itemize}
	\item \textit{Must} support visualisation of Solos Diagrams through a GUI\@.
	\item \textit{Must} support input/output of a (potentially manipulated) Diagram from/to an Expression.
	\item \textit{Should} support manipulation of a Diagram through use of the GUI only.
	\item \textit{Should} be both intuitive and feature-complete to provide an obvious improvement over text-based input as described in Section~\ref{subsec:reqs_solo_expr}.
	\item \textit{May} show step-by-step reductions of Diagrams.
\end{itemize}


\section{Project Plan}
\subsection{Phases}
The project will be broken down into the following stages, with each building on the previous. This allows flexibility if there is not enough time as an incomplete extension will still leave a working system.
\begin{enumerate}
	\item Literature review
	\item Core system (Solos Expressions) implementation and unit testing
	\item Extension 1 (Solos Diagrams) implementation and integration testing
	\item Extension 2 (Graphical Visualisation) implementation and integration testing
	\item Analysis, write-up and conclusion
\end{enumerate}

\subsection{Milestones and Deliverables}
\begin{center}
	\begin{tabular}{ l l }
		\toprule
		\textit{Milestone} & \textit{Target Date} \\
		\midrule
		\textbf{Project proposal} & {27th October, 2017} \\
		\textbf{Literature review} & {24th November, 2017} \\
		{Minimum viable product --- Solos Expressions} & {20th December, 2017} \\
		{Extension 1 --- Solos Diagrams} & {20th January, 2018} \\
		{Extension 2 --- Graphical representation} & {19th February, 2018} \\
		\textbf{Progress demonstration} & {19th February, 2018} \\
		{User testing and critical analysis} & {10th March, 2018} \\
		\textbf{Dissertation} & {4th May, 2018} \\
		\bottomrule
	\end{tabular}
\end{center}

\subsection{Time Breakdown}
\begin{center}
	\begin{ganttchart}[
		hgrid,
		vgrid={*{6}{draw=none}, dotted},
		time slot format=isodate,
		y unit title=0.6cm,
		y unit chart=0.6cm,
		x unit=0.065cm,
		milestone/.append style={xscale=5},
	]{2017-11-01}{2018-04-29}
		\ganttset{bar height=0.6}
		\gantttitlecalendar{year, month=name, week} \\
		\ganttbar{Literature Study}{2017-11-01}{2017-11-24} \\
		\ganttlinkedmilestone{Literature Review}{2017-11-24} \\
		\ganttlinkedbar{Solos Expressions}{2017-11-24}{2017-12-20} \\
		\ganttlinkedbar{Solos Diagrams}{2018-01-07}{2018-01-20} \\
		\ganttlinkedbar{Graphical Interface}{2018-01-20}{2018-02-19} \\
		\ganttlinkedmilestone{Progress Demo}{2018-02-19} \\
		\ganttlinkedbar{Finishing, User Testing}{2018-02-19}{2018-03-10} \\
		\ganttlinkedbar{Critique, Analysis}{2018-03-10}{2018-04-01} \\
		\ganttlinkedbar{Write-Up}{2018-04-01}{2018-04-27} \\
		\ganttlinkedmilestone{Dissertation}{2018-04-29}
	\end{ganttchart}
\end{center}

\section{Resources}
\subsection{Technology}
The system will be developed as a Python application, as this allows for quick, modular development as performance is not critical, but still allows for multiple processes without hassle.
The GUI is likely to be written using Tcl/Tkinter, as it is the common choice for Python graphical interfaces and is suited to 2D visualisation well-enough to be useful, while allowing easy implementation of interactive features.

\subsection{Literature}
Most literature for the project has already been obtained through both digital copies of papers and journals as well as physical books on the subject. While the topic of Solos calculus forms a niche area of research, the study of $\pi$-calculus is well-studied and has a large amount of relevant information that may be carried over.

\subsection{Testing and Evaluation}
In the scope of a learning and teaching aid on the subject, the system will need to be assessed for ease-of-use and for how well it explains the topic. For this, a small group of volunteers with moderate Computer Science understanding must be sourced, likely from a group of peers.


\section{References}
\bibliography{proposal}


%\section{Agreement}
%\subsection{Student Signature}
%\vspace{1in}

%\subsection{Supervisor Signature}
%\vspace{1in}


\end{document}
