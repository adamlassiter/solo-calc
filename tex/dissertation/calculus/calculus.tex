\subsection{Calculus Implementation}\label{ssec:calculus-implementation}
    
    \subsubsection{Overview and Analysis}\label{sssec:calculus-analysis}
        The implementation of reduction semantics and calculus objects was achieved through converting all expressions to a canonical normal form.
        Due to the equivalence properties of the null agent $0$, it was excluded from the implementation as unnecessary.
        First, we must gather some possible structural congruences.

        \begin{lemma}{Structural Equivalence\\}
            For any agents $P, Q, \ldots$ the following structural equivalences hold.
            \begin{align*}
                (x)(y) P                & \equiv (x \, y) P \\
                P \; | \; (Q \; | \; R) & \equiv P \; | \; Q \; | \; R \\
                P \; | \; (x) Q         & \equiv (z)(P \; | \; Q\{z / x\}) \quad \text{where} \quad z \notin names(P) \cup names(Q)
            \end{align*}
            Using these congruences, expressions may be freely reordered through $\alpha$-equivalence only.
        \end{lemma}

        \begin{lemma}{Flattening Theorem\\}\label{flattening-theorem}
            For any agents $P, Q$ the following (non-structural) equivalence holds.
            \begin{align*}
                !(\tilde{x})(P \; | \; !Q) \sim (z)(!(\tilde{x})(P \; | \; z \, \tilde{y}) \; | \; !(\tilde{w})(\bar{z} \, \tilde{w} \; | \; Q\{\tilde{w}/\tilde{y}\})) \quad \text{where} \quad \tilde{y} \defeq fn(Q) \supset \tilde{x}
            \end{align*}
        \end{lemma}
        \begin{corollary*}{Non-Nesting of Replicators\\}
            By induction on $Q$ in Lemma~\ref{flattening-theorem}, any nesting of replicated agents may be flattened to a composition of non-nested replicators.
        \end{corollary*}
        This equivalence amounts to communicating between $P$ and $Q$ each bound name, allowing the two agents to `interact' within their shared scope, $\tilde{y}$ and $\tilde{w}$ respectively, without nesting replications.

        \begin{definition}{Normal Form\\}
            An agent $P$ is of normal form \textit{iff}
            \begin{align*}
                                    P \quad &\defeq \quad (\tilde{x})(\prod_{i}{y_i \, \tilde{z_i}} \; | \; \prod_{j}{! \, Q_j}) \\
                \text{where} \quad Q_j \quad &\defeq \quad (\tilde{x})(\prod_{k}{y_k \, \tilde{z_k}})
            \end{align*}
            That is, P is written as a 3-tuple of a scope, a composition of solos and a composition of replicators, where each replicator is written as a 2-tuple of a scope and a composition of solos only.
        \end{definition}

        \begin{lemma}{~\\}
            For any agent P, $\exists \, Q \sim P$ such that Q is in normal form.
            \begin{proof}
                The proof is trivial by applying recursively the equivalences above.
                See below for an algorithm for such a construction.
            \end{proof}
        \end{lemma}

        \begin{breakablealgorithm}
            \caption{Construction of Normal Forms}
            \begin{algorithmic}[1]
                \Require{Agent $P$ a Normal Form, $Q$ non-Normal Form}
                \Ensure{Agent $P'$, Normal Form of $P$ and $Q$}
                \Function{Normalise}{$P$}
                    \If{$Q$ a Scope}
                        \State$(\tilde{bn})Q' \defeq Q$
                        \State$collisions \defeq \tilde{bn} \cap names(P)$
                        \If{$collisions \neq \emptyset$}
                            \State$\alpha : collisions \rightarrow fresh\;names$
                            \State\Return\Call{Normalise}{$P$, $\alpha(Q)$}
                        \Else
                            \State\Return$(\tilde{bn})$\Call{Normalise}{$P$, $Q'$}
                        \EndIf\\

                    \ElsIf{$Q$ a Composition}
                        \State$Q_1 \;|\; \ldots \;|\; Q_n \defeq Q$
                        \State\Return\Call{Reduce}{\texttt{Normalise}, $P$, $Q_1 \ldots Q_n$}\\

                    \ElsIf{$Q$ a Replication}
                        \State$Q' \defeq$~\Call{Normalise}{$Q''$} where $!Q'' \defeq Q$
                        \If{$replicators(Q') \neq \emptyset$}
                            \State$\bar{Q} \defeq$~\Call{Flatten}{Q}
                            \State\Return\Call{Normalise}{$P$, $\bar{Q}$}
                        \Else
                            \State$collisions \defeq bn(P) \cap bn(Q')$
                            \State$\alpha : collisions \rightarrow fresh\;names$
                            \State\Return$P \; | \; !\,\alpha(Q')$
                        \EndIf\\

                    \ElsIf{$Q$ a Solo}
                        \State$(\tilde{bn})P' \defeq P$
                        \State$collisions \defeq \tilde{bn} \cap names(Q)$
                        \If{$collisions \neq \emptyset$}
                            \State$\alpha : collisions \rightarrow fresh\;names$
                            \State\Return\Call{Normalise}{$\alpha(P)$, $Q$}
                        \Else
                            \State\Return$(\tilde{bn})(P' \; | \; Q)$
                        \EndIf
                    \EndIf
                \EndFunction
            \end{algorithmic}
        \end{breakablealgorithm}
        Normalisation is then performed on an agent $P$ by $\texttt{Normalise}(P) \defeq \texttt{Normalise}(0, P)$.
        It will be seen later that this is exactly equivalent to how Solo Diagrams are represented.
        To ease implementation difficulties later on, the replicators of any normal form are subject to Barendregt's variable naming convention.
        That is, for $P \defeq (\tilde{x})(u \, \tilde{y} \; | \; \ldots \; | \; !Q)$, the bound names of $Q$ are free in $P \setminus !Q$.
        From here onwards, it is assumed that any agent has already been converted to normal form where appropriate.\\

        The algorithm for reductions is a simple search problem for two solos of matching subject and arity (number of names) and opposite parity (input vs. output).
        \begin{breakablealgorithm}
            \caption{Reduction of Solos}
            \begin{algorithmic}[1]
                \Require{Agent $P$}
                \Ensure{Agent $P'$, a reduction of $P$}
                \Function{Reduce}{$P$}
                    \ForEach{$i \in inputs(P)$}
                        \ForEach{$o \in outputs(P)$}
                            \If{$i$ agrees with $o$}
                                \State$\sigma \defeq$ \Call{Fuse}{$i$, $o$, $bn(P)$}
                                \If{$\sigma \neq \textit{none}$}
                                    \State\Return$\sigma(P - \{i, o\})$
                                \EndIf
                            \EndIf
                        \EndFor
                    \EndFor
                    \State\Return$P$
                \EndFunction
            \end{algorithmic}
        \end{breakablealgorithm}

        The next step is to find a suitable $\sigma$, should one exist. This is a renaming of the names of $P$ and must satisfy:
        \begin{align*}
            \sigma : \; & bn(P) \rightarrow names(P) \\
            \emptyset = \; & domain(\sigma) \cap range(\sigma) \\
            \sigma(x) = \; & y \implies \text{$x$ and $y$ have been fused}
        \end{align*}

        This is found through converting the list of pairs of object names to edges of a graph.
        For each disconnected subgraph, or partition, the span of names forms a set which must all be fused into one another.
        Subsequently, there must be at most one free name in this set as two or more would require renaming one free name to another, which is not allowed.
        If no free name exists, a bound name is chosen as the `free name' at random.
        $\sigma$ is then constructed by $\sigma(bn) \defeq fn$.

        \begin{breakablealgorithm}
            \caption{Fusion of Solos}
            \begin{algorithmic}[1]
                \Require{$i_1 \ldots i_n \text{ objects of solo } i, \quad o_1 \ldots o_n \text{ objects of solo } o, \quad bn \; \text{set of bound names}$}
                \Ensure{$\sigma : bn \rightarrow names(i) \cup names(o)$ or \textit{none}}
                \Function{Fuse}{$i_1 \ldots i_n$, $o_1 \ldots o_n$, $bn$}
                    \State$\texttt{Graph} \; g \defeq \{(i_j, o_j) \; | \; 1 \leq j \leq n\}$
                    \State$\texttt{Map} \; \sigma \defeq id$
                    \ForEach{$\texttt{Graph} \; \bar{g} \in partitions(g)$}
                        \State$isect \defeq nodes(\bar{g}) - bn$
                        \If{$|isect| = 0$}
                            \State$fn \defeq x \in nodes(\bar{g})$
                        \ElsIf{$|isect| = 1$}
                            \State$fn \defeq x \in isect$
                        \Else
                            \State\Return\textit{none}
                        \EndIf
                        \ForEach{$name \in nodes(\bar{g}) - \{fn\}$}
                            \State$\sigma(name) \defeq fn$
                        \EndFor
                    \EndFor
                    \State\Return$\sigma$
                \EndFunction
            \end{algorithmic}
        \end{breakablealgorithm}
        
        \begin{examples}{~\\}
            The following serve as a visualisation on the graphs constructed for finding a given $\sigma$.
            \begin{figure}[H]
                \centering
                \begin{tikzpicture}[transform shape, every node/.style={circle, fill=black!100, inner sep=0.05cm}]
                    \coordinate[anchor=center](center);
                    \coordinate[left=2cm of center](centerleft);
                    \node[left=0.86cm of centerleft, label=left:{$a$}](a){};i
                    \node[above=0.5cm of centerleft, label=above:{$b$}](b){};
                    \node[below=0.5cm of centerleft, label=below:{$c$}](c){};
                    \draw[-] (a) -- (b) -- (c) -- (a);
                    
                    \node[above=0.5cm of center, label=above:{$d$}](d){};
                    \node[below=0.5cm of center, label=below:{$f$}](f){};
                    \draw[-] (d) -- (f);

                    \coordinate[right=2cm of center](centerright);
                    \node[above=0.5cm of centerright, label=above:{$e$}](e){};
                    \node[below=0.5cm of centerright, label=below:{$g$}](g){};
                    \draw[-] (e) -- (g);
                \end{tikzpicture}
                \caption*{$\texttt{Fuse}((a, b, c, d, e), \; (b, c, a, f, g)) \rightarrow \{\{a, b, c\}, \{d, f\}, \{e, g\}\}$}
            \end{figure}
            \begin{figure}[H]
                \centering
                \begin{tikzpicture}[transform shape, every node/.style={circle, fill=black!100, inner sep=0.05cm}]
                    \coordinate[anchor=center](center);
                    \coordinate[below=0.5cm of center](fudgefactor);
                    \coordinate[left=2cm of fudgefactor](centerleft);
                    \node[above left=1cm of centerleft, label=above left:{$b$}](lal){};
                    \node[above right=1cm of centerleft, label=above right:{$c$}](lar){};
                    \node[below=1cm of lal, label=below left:{$d$}](lbl){};
                    \node[below=1cm of lar, label=below right:{$e$}](lbr){};
                    \node[above right=1cm of lal, label=above:{$a$}](lac){};
                    \draw[-] (lbl) -- (lal) -- (lac) -- (lar) -- (lbr);
                    \draw[-] (lar) -- (lal);

                    \coordinate[right=2cm of center](centerright);
                    \node[label=above left:{$w$}](rc) at (centerright) {};
                    \node[above=1cm of centerright, label=above:{$u$}](ra){};
                    \node[left=1cm of centerright, label=left:{$v$}](rl){};
                    \node[right=1cm of centerright, label=right:{$x$}](rr){};
                    \node[below=1cm of centerright, label=below:{$z$}](rb){};
                    \node[left=1cm of rb, label=left:{$y$}](rbl){};
                    \draw[-] (ra) -- (rc) -- (rb) -- (rbl);
                    \draw[-] (rl) -- (rc) -- (rr);
                \end{tikzpicture}
                \caption*{$\texttt{Fuse}((a, a, u, b, b, c, v, w, w, y), \; (b, c, w, c, d, e, w, x, z, z)) \rightarrow \{\{a, b, c, d, e\}, \{u, v, w, x, y, z\}\}$}
            \end{figure}
        \end{examples}

        This forms a complete implementation of non-replicating parts of the calculus.
        To avoid the implementation problems with replicators mentioned by~\cite{solo-diagrams}, it is necessary to only perform expansions on replicators that can be reduced.
        While the method described in the aforementioned paper would be suitable, it is enough to simply search for replicators which may be reduced, but only expand them rather than perform a complete reduction.
        This eases some implementation details and minimises code duplication while remaining correct.
        The expansion will be reduced on the next pass of the \texttt{Reduce} function.
        Below shows an extension of the \texttt{Reduce} function for a replicator-replicator fusion (reduction of the form $!P \;|\; !Q$).

        \begin{breakablealgorithm}
            \caption{Reduction of Replicators}
            \begin{algorithmic}[1]
                \Require{Agent $P$}
                \Ensure{Agent $P'$, a reduction of $P$}
                \Function{Reduce}{$P$}
                    \ForEach{$i \in inputs(P)$}
                        \State$\ldots$
                    \EndFor\\

                    \ForEach{$i \in inputs(R_i) \; | \; R_i \in \bigcup_{!R \in P} R$}
                        \ForEach{$o \in outputs(R_o) \; | \; R_o \in \bigcup_{!R \in P} R$}
                            \If{$i$ agrees with $o$}
                                \State$\sigma \defeq$ \Call{Fuse}{$i$, $o$, $bn(P) \cup bn(R_i) \cup bn(R_o)$}
                                \If{$\sigma \neq \textit{none}$}
                                    \State\Return$P \cup \{R_i \; | \; R_o\}$
                                \EndIf
                            \EndIf
                        \EndFor
                    \EndFor
                    \State\Return$P$
                \EndFunction
            \end{algorithmic}
        \end{breakablealgorithm}

        The third case of solo-replicator fusion (reduction of the form $P \;|\; !Q$) is then trivial.


    \subsubsection{Testing and Correctness}
        Testing of this section was integrated with testing of the REPL by using the helper functions it provided for constructing agents.
        An in-depth description can be found in~\ref{sssec:repl-testing}. \\
        
        Early tests could be tested by string equalities and this was sufficient.
        More complicated agents however led to non-deterministic results and non-trivial equalities.
        The first example of this occured while comparing the following agents:
        \begin{align*}
            (x) p \, x \quad\equiv\quad (y) p \, y \quad\neq\quad (x) p \, y
        \end{align*}
        Before testing could be achieved in an automated manner, an algorithm for testing for $\alpha$-equivalence was first required.\\

        It will be seen later that conversion of calculus expressions into diagrams intuitively shows $\alpha$-equivalence is reducible to graph isomorphism, so is expected to be of poor performance.
        \begin{breakablealgorithm}
            \caption{$\alpha$-Equivalence of Agents}
            \begin{algorithmic}[1]
                \Require{Agents $P, Q$}
                \Ensure{Map $\alpha : names(P) \rightarrow names(Q) \textit{ s.t. } \alpha(P) \equiv Q$ or \textit{none}}
                \Function{$\alpha$-Equivalence}{$P$, $Q$}
                    \If{$P \equiv Q$}
                        \State\Return$id$
                    \EndIf
                    \For{$comb_{scope} \in combinations(scope(P), scope(Q))$}
                        \State$\alpha_{P, Q} : x \mapsto y \; \forall \; (x, y) \in comb_{scope}$
                        \For{$comb_{replicator} \in combinations(\{R \; | \; !R \in P\}, \{R \; | \; !R \in Q\})$}
                            \ForEach{$(r_P, r_Q) \in comb_{replicator}$}
                                \State$\alpha_{r_P, r_Q} \defeq$ \Call{$\alpha$-Equivalence}{$r_P, r_Q$}
                            \EndFor
                            \If{$\textit{none} \in \{\alpha_{r_P, r_Q} | (r_P, r_Q) \in comb_{replicator}\}$}
                                \State\textbf{continue for}
                            \ElsIf{$\exists \; \alpha_1, \alpha_2 \in \{\alpha_{r_P, r_Q}\} \textit{ s.t. } \alpha_1 \text{ disagrees with } \alpha_2$}
                                \State\textbf{continue for}
                            \Else
                                \State$\alpha \defeq \alpha_{P, Q} \cup \bigcup_{\alpha \in \{\alpha_{r_P, r_Q}\}} \alpha$
                            \EndIf
                            \If{$\alpha(P) \equiv \alpha(Q)$}
                                \State\Return$\alpha$
                            \EndIf
                        \EndFor
                        \If{$\alpha(P) \equiv \alpha(Q)$}
                            \State\Return$\alpha$
                        \EndIf
                    \EndFor
                    \State\Return\textit{none}
                \EndFunction
            \end{algorithmic}
        \end{breakablealgorithm}
        Note that the algorithm here does not take into account parity of subjects.
        While parities of free subject names do affect $\alpha$-equivalence, those of bound subject names may be reversed.
        That is, while $u \, x \neq \bar{u} \, x$, it does hold that $(u)u \, x \equiv (u)\bar{u} \, x$.


    \subsubsection{Discussion}
        The initial implementation stayed closer to the core concepts of the calculus.
        There existed separate classes of objects for each of Scopes, Compositions, Replications, Matches and Solos.
        While such an approach is perhaps more intuitive, problems arise due to conflicts between how the data structure naturally appears to be tree-like where properties of agents are functions of themselves and their children only and how whether or not a name is bound or free is a property dependent upon an agent or its parent, grandparent etc.

        \begin{example*}
            Consider the following equivalent expressions.
            \begin{align*}
                P \quad & \defeq \quad (y)(a \, x \,|\, (\bar{a} \, y \,|\, p \, y)) \\
                Q \quad & \defeq \quad (a \, x \,|\, (y)(\bar{a} \, y \,|\, p \, y))
            \end{align*}
            Both $P$ and $Q$ have the property that $y$ is a bound name and both should reduce to the term $p \, x$.
            In both cases, $y$ and $x$ are fused into the free name $x$. The scope of $y$ disappears and the remaining $p$ term, which may be thought of as a `print' instruction, displays the result of the fusion.
            Consider now the (inequivalent) expression.
            \begin{align*}
                R \quad & \defeq \quad (a \, x \; | \; (y)\bar{a} \, y \; | \; p \, y)
            \end{align*}
            This term, while similar to $P$ and $Q$, reduces to the term $p \, y$, as our scoped $y$ that is fused is not the same as the free $y$ that is `printed'.
            This can be seen through $\alpha$-equivalence and renaming $(y)\bar{a} \, y$ to the equivalent term $(z)\bar{a} \, z$ and observing that $a$ is a fusion on $x$ and $z$.

            The distinction between the two behaviours is not intuitive, especially in non-trivial cases where agents may be deeply nested.
        \end{example*}

        The above example is further amplified when manipulating expressions symbolically.
        When searching for reducible patterns, the expression is divided and recombined, rearranging the expression tree and subsequently each node's parents.
        While the problem is solvable through updating which node has which parent as the expression is rearranged and manipulated, the solution eventually becomes an attempt to form a normal form out of the expression, leading to the revised method described in~\ref{sssec:calculus-analysis}.\\

        Through the implementation of the calculus, many interesting properties became apparent, with some of the most noticeable listed below.

        \begin{lemma*}{Non-Finite Reductions\\}
            There exist expressions for which reductions may be performed indefinitely.
            For a reduction to be non-finite, both solos must be replicable.
            In particular, expressions of the form $(!(\tilde{x})(u \, \tilde{x} \; | \; P) \; | \; !(\bar{u} \, \tilde{y} \; | \; Q) \; | \; R)$ are non-finite.
        \end{lemma*}

        \begin{example*}{~\\}
            Consider the following expressions, each a fusion of a bound $x$ and free $y$:
            \begin{align*}
                P \rightarrow P'\{y / x\} & \text{ where } x \in \; !(x)Q \in P \text{ and } y \in \; !R \in P \\
                (x)P \rightarrow P'\{y / x\} & \text{ where } x \in \; !Q \in P \text{ and } y \in \; !R \in P
            \end{align*}
            The first expression may be reduced indefinitely, each time adding the expansion of $Q$ to $P$.
            The second may be performed once, after which each $x$ in $Q$ is renamed to $y$.
            It is then ambiguous as to whether it may be performed again --- on one hand the fusion is on two free names, on the other the two names are the same, so there is only one distinct free name.
        \end{example*}

        \begin{remark*}{Behaviour of Trivial Reductions\\}
            For this body of work, since the reduction requires $\sigma : \emptyset \rightarrow \emptyset$, it was assumed that under such conditions a reduction should not be seen to have happened.
        \end{remark*}
