\subsection{Read-Eval-Print-Loop}

    \subsubsection{Overview and Analysis}
        A simple REPL interface was constructed to interact with the code written in~\ref{ssec:calculus-implementation}.
        This was done using extended regular expressions similar to those found in the Perl programming language.
        These do not meet the normal definitions of regular expressions as they additionally support recursive matches, backreferences and various other syntactic additions that make them context-free grammars rather than regular expressions.
        In fact, given a suitable engine for applying context-free grammars, this would be a preferable way to parse expressions. \\

        For each agent, there was an extended regex matching its string representation, with backreferences to extract the necessary data.
        Each of $(?P\langl name \rangl \textit{expression})$ represents a named backreference to the given expression and $(? \, \& rec)$ is a recursive call to the outer $(?\langl rec \rangl \textit{expression})$ expression.
        
        \begin{definition}{Solo Matching\\}
            Solos are built using the regex as follows:
            \begin{align*}
                u \, \tilde{x}       & \quad \longleftarrow \quad \backslash s? (?P \langl subject \rangl [a-z \, 0-9]+) \backslash s (?P \langl objects \rangl ([a-z \, 0-9]+ \backslash s?)+) \backslash s? \\
                \bar{u} \, \tilde{x} & \quad \longleftarrow \quad \backslash s? \backslash ^{\wedge} (?P \langl subject \rangl [a-z \, 0-9]+) \backslash s (?P \langl objects \rangl ([a-z \, 0-9]+ \backslash s?)+) \backslash s?
            \end{align*}
            where a valid name is any lowercase alphanumeric word and $\bar{u}$ is inputted as \texttt{$^{\wedge}$u} and each of \texttt{subject} and \texttt{objects} represent the subject and objects of the solo respectively.
        \end{definition}

        \begin{definition}{Replication\\}
            Replicators are built using the regex as follows:
            \begin{align*}
                ! P \quad \longleftarrow \quad \backslash s? \; !(?P \langl agent \rangl.*)\backslash s?
            \end{align*}
            where \texttt{agent} represents $P$ for the expression $!P$.
        \end{definition}

        \begin{definition}{Scope\\}
            Scopes are built using the regex as follows:
            \begin{align*}
                (x)P \quad \longleftarrow \quad \backslash s? \backslash ((?P \langl bindings \rangl([a-z \, 0-9]+ \backslash s?)+)\backslash )(?P\langl agent \rangl[^{\wedge}\backslash s].+)\backslash s?
            \end{align*}
            where each of \texttt{bindings} and \texttt{agent} represent $x$ and $P$ respectively for the expression $(x) P$.
        \end{definition}

        \begin{definition}{Composition\\}
            Compositions are built using the recursive regex as follows:
            \begin{align*}
                P_1 \, | \ldots | \, P_n \quad \longleftarrow \quad & \backslash s? \backslash (?\langl agents\rangl~\texttt{R}+) (\backslash | (?\&agents))?)\backslash )\backslash s? \\
                \text{where} \quad \texttt{R} \quad \defeq \quad & (?\langl agent\rangl([^{\wedge}|()] | (?\langl rec\rangl\backslash ((?:[^{\wedge}()]++|(?\&rec))*\backslash )))
            \end{align*}
            where each \texttt{agent} is collected in \texttt{agents} and represents each of $P_1 \ldots P_n$.
        \end{definition}
        It is accepted that this is difficult to read and would benefit from a better-structured parsing system.
        For this project and as a write-once solution, it is suitable enough.


    \subsubsection{Testing and Correctness}\label{sssec:repl-testing}
        Testing was first done of the structural equivalence and equality of expressions.
        First, for each of the congruences described above, a normalised expression and its known normal form were tested for equality.
        Testing of reductions was done through testing each of the four cases of fusions being performed:
        \begin{itemize}
            \item Normal fusion $(\tilde{x})(a \tilde{x} \,|\, \bar{a} \, \tilde{y})$
            \item Cross-replicator fusion $(\tilde{x})(a \, \tilde{x} \,|\, !(\bar{a} \, \tilde{y}))$
            \item Multi-replicator fusion $(\tilde{x})(!(a \, \tilde{x}) \,|\, !(\bar{a} \, \tilde{y}))$
            \item Inter-replicator fusion $(\tilde{x})(!(a \, \tilde{x} \,|\, \bar{a} \, \tilde{y}))$
        \end{itemize} 
        where for each, there exist approximately four cases of which variables are bound and where the matching scope lies.
        %% TODO: Finally, a test of an expression containing all of the above agents was reduced and checked for correctness.

        Due to the open-endedness of the problem, it is difficult to provide a conclusive, complete testing suite.
        In particular, checking for false positives presents a large space of possible tests.
        This was managed by reducing to the above unit tests and combining with the integration test of each case of fusion.\\

        A sample of test outputs can be found in~\ref{ssec:appendix-calculus-tests}


    \subsubsection{Discussion}
        No research was done as to the usability of this interface.
        Future work would vastly improve the description provided here, particularly with regards to error-reporting.
        Another notable flaw is a disconnect between text inputted and outputted, for example \textit{$^{\wedge}$p a b c} $\rightarrow$ \textit{$\bar{p}$ a b c}.\\

        With further work on the project, I would have hoped to embed this REPL interface in the REST server described in~\ref{ssec:rest-server}.
        Through this, the diagram presented by the server could be changed on the server-side.
