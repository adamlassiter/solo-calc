\subsection{Diagrams Implementation}

    \subsubsection{Overview and Analysis}\label{sssec:diagram-implementation-analysis}
        The implementation of diagrams was done through the definitions provided in~\cite{solo-diagrams}.
        Beginning with the definition of a complete diagram, an associated class was written for each object and structure using the definitions as written in~\ref{ssec:diagram-definitions}.
        There was one small change to these definitions, where a diagram did not have an associated labelling $l$.
        This was instead implemented within the nodes themselves, which were uniquely identified by a UUID4 string but also had a separate (possibly \textit{none}) \texttt{name} property.
        This change meant graphs could have equivalently-named nodes that were functionally distinct. \\

        First, a quick recap of the revised definitions.

        \begin{definition}{Multiset\\}
            A multiset $M$ is an unordered collection of elements $x_1 \ldots x_n$.
            The elements $X_i$ need not be distinct.
            In particular, $M$ is a map relating each element of the underlying set to a positive integer.
        \end{definition}

        \begin{definition}{Diagram\\}
            A diagram $D$ is a 2-tuple $(G, M)$ where $G$ is a graph and $M$ a multiset of boxes.
        \end{definition}

        \begin{definition}{Graph\\}
            A graph $G$ is a multiset of edges.
        \end{definition}

        \begin{definition}{Box\\}
            A box $B$ is a 2-tuple $(G, S)$ where $G$ is a graph and $S \subset nodes(G)$ set of internal nodes.
            $S$ is the internal nodes of $B$ and $nodes(G) \setminus S$ the principal or perimeter nodes.
        \end{definition}

        \begin{definition}{Edge\\}
            An edge $E$ is a k-tuple of names $(x, [x_1, \, \ldots, \, x_{k-1}])$ and a parity $p \in \{in, \, out\}$ where $x$ is the subject of $E$ and $x_1, \, \ldots, \, x_{k-1}$ the objects.
            $E$ is then written $<x, x_1, \cdots, x_{k-1}>_p$
            Note the case $k = 1$ where $E$ has no objects.
        \end{definition}

        \begin{definition}{Node\\}
            A node $x$ is a unique identifier \texttt{uuid} and optionally a labelling \texttt{name}.
        \end{definition}

        The implementation of these definitions as a data structure is trivial.\\

        The implementation could end here as there exists an isomorphism between diagrams and calculus expressions as follows:
        \begin{lemma}{Isomorphism of Diagrams and Expressions\\}
            For any diagram $D \defeq (G, M \defeq (G', S))$, there exists a calculus expression $P$. In particular, this $P$ is unique up to normal form.
            \begin{align*}
                P \defeq (\{x \in names(D) \; | \; x.\texttt{name} = \textit{none}\})(\bigcup_{e \, \in \, edges(D \setminus M)}{e} \quad | \quad \bigcup_{b \in M}{!Q \text{ where } Q \equiv (G', \emptyset)})
            \end{align*}
        \end{lemma}
        As it can be seen, this is exactly the normal form defined in~\ref{sssec:calculus-analysis}.
        This bijection between a normal form of calculus expressions and graph-like diagrams leads to a useful result.

        \begin{corollary*}{Hardness of $\alpha$-Equivalence Problem\\}
            The general case of $\alpha$-equivalence is equivalent to graph-isomorphism.
        \end{corollary*}
        
        With this in mind, the poor worst-case performance of the aforementioned $\alpha$-equivalence algorithm is known to be unavoidable.

        \begin{breakablealgorithm}
            \caption{Fusion of Nodes}
            \begin{algorithmic}[1]
                \Require$e_i, e_o$ edges of matching subject and opposite parity $\in D$ a diagram
                \Ensure$\sigma : labelled(D) \defeq \{x \in nodes(D) \; | \; x.\texttt{name} = \textit{none}\} \rightarrow nodes(D)$ or \textit{none}
                \Function{Fuse}{$e_i$, $e_o$}
                    \State$x_1, \ldots, x_n \defeq objects(e_i)$
                    \State$y_1, \ldots, y_n \defeq objects(e_o)$
                    \State$\texttt{Graph} \; g \defeq \{(x_i, y_i) \; | \; 1 \leq i \leq n\}$
                    \State$\texttt{Map} \; \sigma \defeq id$
                    \ForEach{$\texttt{Graph} \; \bar{g} \in partitions(g)$}
                        \State$isect \defeq nodes(\bar{g}) \cap labelled(D)$
                        \If{$|isect| = 0$}
                            \State$fn \defeq x \in nodes(\bar{g})$
                        \ElsIf{$|isect| = 1$}
                            \State$fn \defeq x \in isect$
                        \Else
                            \State\Return\textit{none}
                        \EndIf
                        \ForEach{$name \in nodes(\bar{g}) - \{fn\}$}
                            \State$\sigma(name) \defeq fn$
                        \EndFor
                    \EndFor
                    \State\Return$\sigma$
                \EndFunction
            \end{algorithmic}
        \end{breakablealgorithm}

        From here, reduction of diagrams is just a simple change of naming conventions from~\ref{sssec:calculus-analysis}.
        Additionally, the stricter structure means there are no edge-cases to consider and box reductions are performed in full, rather than expanding and fusing non-box edges.
        Again, the following algorithm describes reduction of two edges outside any boxes and of two solos both inside boxes.
        The other cases of reduction are trivial to deduce.

        \begin{breakablealgorithm}
            \caption{Reduction of Graphs}
            \begin{algorithmic}[1]
                \Require$D$ a diagram
                \Ensure$D'$ a reduction of $D$
                \Function{Reduce}{$D$}
                    \ForEach{$\alpha \in edges(D)$}
                        \ForEach{$\beta \in \{e \in edges(D) \; | \; parity(e) \neq parity(\alpha)\}$}
                            \If{$\alpha \text{ agrees with } \beta$}
                                \State$\sigma \defeq$~\Call{Fuse}{$\alpha$, $\beta$}
                                \State\Return$\sigma(D \setminus \{\alpha, \beta\})$
                            \EndIf
                        \EndFor
                    \EndFor\\

                    \ForEach{$\alpha \in B_{\alpha} \in boxes(D)$}
                        \ForEach{$\beta \in B_{\beta} \in boxes(D)$}
                            \If{$\alpha \text{ agrees with } \beta$}
                                \State$\rho : internals(B_{\alpha} \; | \; B_{\beta}) \rightarrow fresh\;nodes$
                                \State$\sigma \defeq \rho($\Call{Fuse}{$\alpha$, $\beta$}$)$
                                \State$G \defeq graph(D) \; | \; graph(B_{\alpha}) \setminus \{\alpha\} \; | \; graph(B_{\beta}) \setminus \{\beta\}$
                                \State$M \defeq boxes(D)$
                                \State\Return$(\sigma(G), \sigma(M))$
                            \EndIf
                        \EndFor
                    \EndFor
                \EndFunction
            \end{algorithmic}
        \end{breakablealgorithm}
        
    \subsubsection{Testing and Correctness}
        Due to the more abstract nature of diagram objects when compared to calculus expressions, testing was combined with testing of the visualiser.
        By default, a diagram with parts allowing for each kind of fusion was produced and examined through each reduction.
        
    \subsubsection{Discussion}
        Similarly to the calculus, there exist diagrams that have a non-finite amount of reductions.
        The behaviour upon reaching a non-finite reduction was left as-is --- the diagram would be left to reduce forever even if there were other reducible parts.
        This could be changed in future implementations to either not perform non-finite reductions if that same reduction was performed recently, or to select edges for reduction with an element of random choice.
        As is, while technically correct, there are some reductions which are never made despite being possible.\\

        When later combined with the REST server for computer interaction, a collection of functions were written to convert diagrams to JSON objects.
        Each of these objects held a unique identifier and a collection of that object's properties.
        The addition of unique identifiers to objects that didn't necessarily require them allowed for improvements in visualisation to be described later.\\

        The most notable difference between the implementation of diagrams and calculus expressions is that the rigid structure of diagrams leaves fewer edge-cases to deal with.
        Overall, the implementation was far easier and less prone to bugs, \textit{whether or not this was because of my experience in implementing the calculus prior to the diagrams or because of a superior design.}
