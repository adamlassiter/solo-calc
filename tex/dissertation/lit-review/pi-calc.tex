\subsection{\texorpdfstring{$\pi$-c}{Pi C}alculus}\label{subsec:pi-calculus}

    The $\pi$-calculus was designed by Robin Milner, Joachim Parrow and David Walker in 1992 as an extension of Milner's work on CCS.
    It was supposed to remain similar to the $\lambda$-calculus as described in~\ref{ssec:lambda-calculus} and improve on CCS by allowing channel names themselves to be sent across channels.


    \begin{definition}{Syntax\\}
        Explained here by~\cite{pi-calculus-intro}\footnotemark, the $\pi$-calculus is constructed from the recursive definition of an expression $P$:
        \footnotetext{See~\cite{pi-calculus} for the original definitions and semantics. The following are functionally equivalent and \textit{I personally believe that the further research on the topic has led to improved definitions and descriptions of the subject.}}
        \begin{center} % revise from ccs document? %
            \begin{tabular}{ l l l }
                $P \quad \defeq$    & $Q \; | \; R$         & (concurrency) \\
                                    & $Q + P$               & (choice) \\
                                    & $c(x).Q$              & (input prefix) \\
                                    & $\bar{c}(x).Q$        & (output prefix) \\
                                    & $\tau.P$              & (silent prefix) \\
                                    & if $x = y$ then $P$   & (match) \\
                                    & $vx.Q$                & (restriction)~\footnotemark\\
                                    & $Q!$                  & (replication)~\footnotemark\\
                                    & $\textbf{0}$          & (null process) \\
            \end{tabular}
        \end{center}
        \addtocounter{footnote}{-1}\footnotetext{This explicitly declares x as local within Q.}
        \stepcounter{footnote}\footnotetext{Replication is defined in theory as $P! \defeq{} P \; | \; P!$. This causes problems in computation as to how much to replicate and is in fact computed differently.}
        where all operations are as found in CCS.
        The notable difference is allowing the sending and receiving of channel names over a channel, similar to passing pointers in traditional programs.
    \end{definition}
    It is worth noting that the match and choice operators are not strictly needed, but are often included as they are usually required to be defined within the calculus anyway.
    The lack of the relabelling operator from CCS (written $\{b / a\}$) may be surprising at first, but the same can be achieved through a definition.
    That is, $P\{a'/a, b'/b\}$ can be recreated within the $\pi$-calculus by \textit{identifying} $P(x, y)$ with $P\{x/a, y/b\}$ in a fashion similar to C-style functions.
    This is simply for the purpose of saving an otherwise unnecessary operator.


    \begin{example*}
        Suppose a process $P$ needs to send multiple names $a, b, c \ldots$ to another process $Q$, but there exist multiple such $Q$s that are all listening on the same channel.
        The naive approach may lead to $Q_1$ receiving $a$, $Q_2$ receiving $b$ etc\ldots
        $P$ can begin by transmitting a private channel name $p$, then transmitting each $a, b, c \ldots$ on $p$:
        \begin{align*}
            (vp) \, \bar{c} p \, . \, \bar{p} a \, . \, \bar{p} b \, . \, \bar{p} c \, . \, P
        \end{align*}
        Then $Q$ must prepare to receive a name then $a, b, c \ldots$ along that named channel, binding each to any desired name:
        \begin{align*}
            c(p) \, . \, p(x) \, . \, p(y) \, . \, p(z) \, . \, Q
        \end{align*}
    \end{example*}
    Note how a generalisation of this could allow $P$ to send a pair of names $(x, a)$ and $Q$ could bind the value $a$ to the name $x$, allowing for hash-map or set-like behaviour.
    \textit{In my opinion, the simplicity of this encoding and the expressiveness it may provide is outstanding.}


    \begin{remarks}
        Built mostly on the work of Milner and CCS, the $\pi$-calculus demonstrated that a simplification in design can (and usually does) lead to a more expressive language.
        There is not much else noteworthy except for the increased ease of use of the calculus over CCS --- the encoding of any examples in \ref{ssec:lambda-calculus} is left as an exercise to the reader in either $\pi$-calculus or CCS.
        While it provides the expressiveness required for Turing-completeness, it does not lend itself to understandability nor clarity of the problem encoding when presented as a standalone expression within the calculus, unless the reader is well-acquainted with the subject in advance.
        Furthermore, larger expressions quickly become unreadable without liberal use of identifiers.
    \end{remarks}
